<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103121433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE feed (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, published_id VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, authors JSON DEFAULT NULL, links JSON DEFAULT NULL, categories JSON DEFAULT NULL, contributors JSON DEFAULT NULL, generator JSON DEFAULT NULL, icon VARCHAR(255) DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, rights VARCHAR(255) DEFAULT NULL, subtitle LONGTEXT DEFAULT NULL, feed_attributes JSON DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_234044ABA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feed_entry (id INT AUTO_INCREMENT NOT NULL, feed_id INT DEFAULT NULL, published_id VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, authors JSON DEFAULT NULL, content JSON DEFAULT NULL, link JSON DEFAULT NULL, summary JSON DEFAULT NULL, categories JSON DEFAULT NULL, contributors JSON DEFAULT NULL, rights VARCHAR(255) DEFAULT NULL, source JSON DEFAULT NULL, published_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_DEAECECC35BA678D (published_id), INDEX IDX_DEAECECC51A5BC03 (feed_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_feed_entry (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, feed_entry_id INT NOT NULL, rate DOUBLE PRECISION NOT NULL, INDEX IDX_79C90820A76ED395 (user_id), INDEX IDX_79C908209AE3DEE5 (feed_entry_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE feed ADD CONSTRAINT FK_234044ABA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE feed_entry ADD CONSTRAINT FK_DEAECECC51A5BC03 FOREIGN KEY (feed_id) REFERENCES feed (id)');
        $this->addSql('ALTER TABLE user_feed_entry ADD CONSTRAINT FK_79C90820A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_feed_entry ADD CONSTRAINT FK_79C908209AE3DEE5 FOREIGN KEY (feed_entry_id) REFERENCES feed_entry (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE feed_entry DROP FOREIGN KEY FK_DEAECECC51A5BC03');
        $this->addSql('ALTER TABLE user_feed_entry DROP FOREIGN KEY FK_79C908209AE3DEE5');
        $this->addSql('ALTER TABLE feed DROP FOREIGN KEY FK_234044ABA76ED395');
        $this->addSql('ALTER TABLE user_feed_entry DROP FOREIGN KEY FK_79C90820A76ED395');
        $this->addSql('DROP TABLE feed');
        $this->addSql('DROP TABLE feed_entry');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_feed_entry');
    }
}
