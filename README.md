####
# Symfony Atom Feed Reader
## Notice: The bundle is here -->  lib/atom/FeedBundle                               
This bundle is a simple reusable feed reader that currently supports Atom feeds. 

This bundle does not depend on a specific framework, so you can use it with any PHP framework.
Currently, this bundle supports FileDriver, CurlDriver, but it is possible to add more driver like
 GuzzleHttp, Symfony HTTP-client just by adding another driver.
 
## Requirements

* PHP 7.2.5+
* Doctrine ORM

## Setup

### Create database tables

Update your database by executing this command from your Symfony root directory:

```bash
$ php bin/console doctrine:migrations:migrate
```

### Run database fixtures (to insert some fake user)

Update your database by executing this command from your Symfony root directory:

```bash
$ php bin/console doctrine:fixtures:load
```

### Clear Caches

```bash
$ php bin/console cache:clear --no-warmup 
```

### Test (After run fixtures)
includes Unit test, Browser test, Database Test
```bash
$ ./vendor/bin/phpunit lib/src/FeedBundle/Tests/ 
```
