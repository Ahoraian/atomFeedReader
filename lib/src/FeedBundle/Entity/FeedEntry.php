<?php

namespace Atom\FeedBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\Cache;
use Atom\FeedBundle\Repository\FeedEntryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FeedEntryRepository::class)
 * @Cache(usage="NONSTRICT_READ_WRITE", region="entity_with_short_cached")
 */
class FeedEntry
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Feed::class, inversedBy="feedEntries")
     */
    protected $feed;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $publishedId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $authors = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $content = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $link = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $summary = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $categories = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $contributors = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $rights;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $source = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publishedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=UserFeedEntry::class, mappedBy="feedEntry", fetch="EXTRA_LAZY")
     */
    protected $userFeedEntries;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->userFeedEntries = new ArrayCollection();
    }

    public function getFeed(): ?Feed
    {
        return $this->feed;
    }

    public function setFeed(?Feed $feed): self
    {
        $this->feed = $feed;

        return $this;
    }

    public function getPublishedId(): ?int
    {
        return $this->publishedId;
    }

    public function setPublishedId(string $publishedId): self
    {
        $this->publishedId = $publishedId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthors(): ?array
    {
        return $this->authors;
    }

    public function setAuthors(?array $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function setContent(?array $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLink(): ?array
    {
        return $this->link;
    }

    public function setLink(?array $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getSummary(): ?array
    {
        return $this->summary;
    }

    public function setSummary(?array $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getCategories(): ?array
    {
        return $this->categories;
    }

    public function setCategories(?array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getContributors(): ?array
    {
        return $this->contributors;
    }

    public function setContributors(?array $contributors): self
    {
        $this->contributors = $contributors;

        return $this;
    }

    public function getRights(): ?string
    {
        return $this->rights;
    }

    public function setRights(?string $rights): self
    {
        $this->rights = $rights;

        return $this;
    }

    public function getSource(): ?array
    {
        return $this->source;
    }

    public function setSource(?array $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|UserFeedEntry[]
     */
    public function getUserFeedEntries(): Collection
    {
        return $this->userFeedEntries;
    }

    /**
     * @param $userId
     * @return Collection|UserFeedEntry[]
     */
    public function getRate(int $userId): Collection
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('user', $userId));

        return $this->userFeedEntries->matching($criteria);
    }

    public function addUserFeedEntry(UserFeedEntry $userFeedEntry): self
    {
        if (!$this->userFeedEntries->contains($userFeedEntry)) {
            $this->userFeedEntries[] = $userFeedEntry;
            $userFeedEntry->setFeedEntry($this);
        }

        return $this;
    }

    public function removeUserFeedEntry(UserFeedEntry $userFeedEntry): self
    {
        if ($this->userFeedEntries->removeElement($userFeedEntry)) {
            // set the owning side to null (unless already changed)
            if ($userFeedEntry->getFeedEntry() === $this) {
                $userFeedEntry->setFeedEntry(null);
            }
        }

        return $this;
    }
}
