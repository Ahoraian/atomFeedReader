<?php

namespace Atom\FeedBundle\Entity;

use Doctrine\ORM\Mapping\Cache;
use Atom\FeedBundle\Repository\UserFeedEntryRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;

/**
 * @ORM\Entity(repositoryClass=UserFeedEntryRepository::class)
 * @Cache(usage="NONSTRICT_READ_WRITE", region="entity_with_short_cached")
 */
class UserFeedEntry
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userFeedEntries", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity=FeedEntry::class, inversedBy="userFeedEntries", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $feedEntry;

    /**
     * @ORM\Column(type="float")
     */
    protected $rate;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFeedEntry(): ?FeedEntry
    {
        return $this->feedEntry;
    }

    public function setFeedEntry(?FeedEntry $feedEntry): self
    {
        $this->feedEntry = $feedEntry;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}
