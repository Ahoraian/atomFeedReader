<?php

namespace Atom\FeedBundle\Entity;

use Doctrine\ORM\Mapping\Cache;
use Atom\FeedBundle\Repository\FeedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User;

/**
 * @ORM\Entity(repositoryClass=FeedRepository::class)
 * @Cache(usage="NONSTRICT_READ_WRITE", region="entity_with_short_cached")
 */
class Feed
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userFeeds")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $publishedId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Url(
     *    relativeProtocol = true,
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $authors = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $links = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $categories = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $contributors = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $generator = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rights;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $subtitle;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $feedAttributes = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=FeedEntry::class, mappedBy="feed", fetch="EXTRA_LAZY")
     */
    private $feedEntries;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->feedEntries = new ArrayCollection();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPublishedId(): ?string
    {
        return $this->publishedId;
    }

    public function setPublishedId(string $publishedId): self
    {
        $this->publishedId = $publishedId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAuthors(): ?array
    {
        return $this->authors;
    }

    public function setAuthors(?array $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

    public function getLinks(): ?array
    {
        return $this->links;
    }

    public function setLinks(?array $links): self
    {
        $this->links = $links;

        return $this;
    }

    public function getCategories(): ?array
    {
        return $this->categories;
    }

    public function setCategories(?array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getContributors(): ?array
    {
        return $this->contributors;
    }

    public function setContributors(?array $contributors): self
    {
        $this->contributors = $contributors;

        return $this;
    }

    public function getGenerator(): ?array
    {
        return $this->generator;
    }

    public function setGenerator(?array $generator): self
    {
        $this->generator = $generator;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getRights(): ?string
    {
        return $this->rights;
    }

    public function setRights(?string $rights): self
    {
        $this->rights = $rights;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getFeedAttributes(): ?array
    {
        return $this->feedAttributes;
    }

    public function setFeedAttributes(?array $feedAttributes): self
    {
        $this->feedAttributes = $feedAttributes;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|FeedEntry[]
     */
    public function getFeedEntries(): Collection
    {
        return $this->feedEntries;
    }

    public function addFeedEntry(FeedEntry $feedEntry): self
    {
        if (!$this->feedEntries->contains($feedEntry)) {
            $this->feedEntries[] = $feedEntry;
            $feedEntry->setFeed($this);
        }

        return $this;
    }

    public function removeFeedEntry(FeedEntry $feedEntry): self
    {
        if ($this->feedEntries->removeElement($feedEntry)) {
            // set the owning side to null (unless already changed)
            if ($feedEntry->getFeed() === $this) {
                $feedEntry->setFeed(null);
            }
        }

        return $this;
    }
}
