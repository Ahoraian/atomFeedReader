<?php

namespace Atom\FeedBundle\Tests\Controllers;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Atom\FeedBundle\Tests\AbstractTestCase;

class UserFeedControllerTest extends AbstractTestCase
{
    const ATOM_URL = 'https://rss.dw.com/atom/rss-en-all';
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $client;

    public function setUp(): void
    {
        $this->client = static::createClient();

        $this->login('user1@example.com');
    }

    /**
     * Test fetch a feed url
     * need to login
     */
    public function testFetchAtomFeed()
    {
        // test e.g. the profile page
        $crawler = $this->client->request('GET', '/user/feed/new');
        $this->assertResponseIsSuccessful();

        $buttonCrawlerNode = $crawler->selectButton('Submit');

        $form = $buttonCrawlerNode->form([
            'feed_form[url]'    => self::ATOM_URL
        ], 'POST');

        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect('/feeds'));
    }
}
