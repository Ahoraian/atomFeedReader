<?php

namespace Atom\FeedBundle\Tests\Controllers;

use Atom\FeedBundle\Tests\AbstractTestCase;

class UserFeedEntryControllerTest extends AbstractTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $client;

    public function setUp(): void
    {
        $this->client = static::createClient();

        $this->login('user1@example.com');
    }

    /**
     * Test fetch a feed url
     * need to login
     */
    public function testRate()
    {
        // test e.g. the profile page
        $crawler = $this->client->request('GET', '/feeds');
        $this->assertResponseIsSuccessful();

        // check rate buttons exists
        $rate = $crawler->filter('div.rating')
            ->eq(5)
            ->filter('input[type="radio"][value="4"]');

        $this->assertGreaterThan(0, $rate->attr('data-id'));

        // check ajax [store rate]
        $this->client->xmlHttpRequest('POST', $rate->attr('data-url'), [
            'id' => $rate->attr('data-id'),
            'rate' => $rate->attr('value')
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);
    }

}
