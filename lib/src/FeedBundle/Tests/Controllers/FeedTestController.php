<?php

namespace Atom\FeedBundle\Tests\Controllers;

use Atom\FeedBundle\Tests\AbstractTestCase;

class FeedTestController extends AbstractTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * GEt feeds webpage in browser
     */
    public function testFeedLoad()
    {
        $this->client->request('GET', '/feeds');

        $this->assertResponseIsSuccessful();
    }
}
