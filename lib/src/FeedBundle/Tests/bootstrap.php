<?php

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Filesystem\Filesystem;
use Atom\FeedBundle\Tests\AppKernel;

$file = __DIR__.'/../vendor/autoload.php';
if (!file_exists($file)) {
    throw new RuntimeException('Install dependencies to run test suite.');
}
$autoload = require $file;

require_once __DIR__ . '/Kernel/AppKernel.php';

(static function(){
    $fs = new Filesystem();

    // Remove build dir files
    if (is_dir(__DIR__.'/../build')) {
        echo "Removing files in the build directory.\n".__DIR__."\n";
        try {
            $fs->remove(__DIR__.'/../build');
        } catch (Exception $e) {
            fwrite(STDERR, $e->getMessage());
        }
    }

    $kernel = new AppKernel('test', true);
    $kernel->boot();

    $databaseFile = $kernel->getContainer()->getParameter('database_path');

    if ($fs->exists($databaseFile)) {
        $fs->remove($databaseFile);
    }

    $application = new Application($kernel);
    $application->setAutoExit(false);
    $application->run(new ArrayInput(['command' => 'doctrine:database:create']));
    $application->run(new ArrayInput(['command' => 'doctrine:schema:create']));

    $kernel->shutdown();
})();
