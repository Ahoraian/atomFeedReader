<?php

namespace Atom\FeedBundle\Tests;

use App\Repository\UserRepository;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractTestCase extends WebTestCase
{
    public function setUp(): void
    {
        static::bootKernel();

        $c = static::$container->get(Connection::class);

        $c->query('select * from feed where 1');

        static::ensureKernelShutdown();

    }

    /**
     * Login
     * @param $username
     */
    protected function login($username)
    {
        $userRepository = static::$container->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail($username);

        // simulate $testUser being logged in
        $this->client->loginUser($testUser);
    }
}
