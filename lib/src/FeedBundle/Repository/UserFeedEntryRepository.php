<?php

namespace Atom\FeedBundle\Repository;

use Atom\FeedBundle\Entity\UserFeedEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserFeedEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFeedEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFeedEntry[]    findAll()
 * @method UserFeedEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFeedEntryRepository extends AbstractFeedRepository
{
}
