<?php

namespace Atom\FeedBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AbstractFeedRepository extends EntityRepository
{
    /**
     * @var bool
     */
    protected $cacheEnabled = false;

    /**
     * @var int
     */
    protected $cacheTtl;

    /**
     * @param array $configs
     */
    public function setConfig(array $configs)
    {
        $this->cacheEnabled = $configs['enabled'];
        $this->cacheTtl     = $configs['ttl'];
    }
}
