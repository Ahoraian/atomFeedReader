<?php

namespace Atom\FeedBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Tools\Pagination\CountWalker;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Atom\FeedBundle\Entity\FeedEntry;

/**
 * @method FeedEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeedEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeedEntry[]    findAll()
 * @method FeedEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedEntryRepository extends AbstractFeedRepository
{
    /**
     * get all feed entries
     * @param $userId
     * @param array|null $filters
     * @param array|null $sortFields
     * @param int $page
     * @param int $limit
     * @return Paginator
     */
    public function paginate(?int $userId = null, ?array $filters = null, ?array $sortFields = null, int $page, int $limit = 12): Paginator
    {
        $query = $this->createQueryBuilder('fe')
            ->distinct('fe.id');

        if ($userId) {
            $query->leftJoin('fe.userFeedEntries', 'ufe', Join::WITH, "ufe.user = :userId")
                ->setParameter('userId', $userId)
                ->addSelect('ufe');
        }

        // set where
        if ($filters) {
            foreach ($filters as $name => $value) {
                $query->andWhere($name . ' = :val')
                    ->setParameter('val', $value);
            }
        }

        // set orders
        if ($sortFields) {
            foreach ($sortFields as $name => $value) {
                $query->orderBy($name, $value);
            }
        }

        // set limit
        $query = $query->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        if ($this->cacheEnabled) {
            $query = $query->getQuery()
                ->useQueryCache(true)
                ->setCacheable(true)
                ->enableResultCache($this->cacheTtl);
        }

        $query->setHint(CountWalker::HINT_DISTINCT, false);
        $result = new Paginator($query);
        $result->setUseOutputWalkers(null);

        return $result;
    }
}
