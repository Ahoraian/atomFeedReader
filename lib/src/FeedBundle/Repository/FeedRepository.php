<?php

namespace Atom\FeedBundle\Repository;

use Atom\FeedBundle\Entity\Feed;
use Atom\FeedBundle\Entity\FeedEntry;
use Atom\FeedBundle\Entity\UserFeedEntry;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Atom\FeedBundle\Repository\AbstractFeedRepository;

/**
 * @method Feed|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feed|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feed[]    findAll()
 * @method Feed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedRepository extends AbstractFeedRepository
{
    /**
     * Return all feed
     * @param array $fields
     * @param array $sortFields
     * @return mixed
     */
    public function first(array $fields = [], array $sortFields = [])
    {
        $query = $this->createQueryBuilder('f');

        // set where
        foreach ($fields as $name => $value) {
            $query->andWhere("f.{$name} = :val")
                ->setParameter('val', $value);
        }
        // set orders
        foreach ($sortFields as $name => $value) {
            $query->orderBy("f.{$name}", $value);
        }

        return $query->getFirstResult();
    }
}
