<?php

namespace Atom\FeedBundle\Controller;

use Atom\FeedBundle\Entity\FeedEntry;
use Atom\FeedBundle\Entity\UserFeedEntry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;;

class UserFeedEntryController extends AbstractFeedController
{
    /**
     * @Route("/user/feed/rate", name="atom_feed_entry_rate", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function rate(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $manager = $this->getDoctrine()->getManager();
        $feedEntryRepo = $manager->getRepository(FeedEntry::class);
        $ufEntryRepo = $manager->getRepository(UserFeedEntry::class);

        $feedEntry = $feedEntryRepo->find($data['id']);

        if (!$feedEntry) {
            return new JsonResponse(['feed entry not found'], 422);
        }

        // Check that the user has already rated this feed entry
        $userFeedEntry = $ufEntryRepo->findOneBy([
            'user' => $this->getUser(),
            'feedEntry' => $feedEntry
        ]);

        $userFeedEntry = $userFeedEntry ?? new UserFeedEntry;;
        $userFeedEntry->setUser($this->getUser());
        $userFeedEntry->setFeedEntry($feedEntry);
        $userFeedEntry->setRate($data['rate']);
        $manager->persist($userFeedEntry);
        $manager->flush();

        return new JsonResponse(['thank you for your vote'], 201);
    }
}
