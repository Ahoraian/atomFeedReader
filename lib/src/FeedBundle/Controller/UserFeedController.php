<?php

namespace Atom\FeedBundle\Controller;

use Ahoraian\Feed\Reader;
use Ahoraian\Feed\Reader\Driver\CurlDriver;
use Ahoraian\Feed\Reader\Exception\NotModifiedResponseException;
use DateTime;
use Atom\FeedBundle\Entity\FeedEntry;
use Atom\FeedBundle\Form\FeedFormType;
use Atom\FeedBundle\Repository\FeedRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface as EntityManagerInterfaceAlias;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserFeedController extends AbstractFeedController
{
    /**
     * @Route("/user/feed/new", name="atom_new_feed")
     * @param EntityManagerInterfaceAlias $em
     * @param FeedRepository $feedRepo
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function new(EntityManagerInterfaceAlias $em, FeedRepository $feedRepo, Request $request)
    {
        $form = $this->createForm(FeedFormType::class);
        $form->handleRequest($request);

        // validate form
        if ($form->isSubmitted() && $form->isValid()) {
            $feed = $form->getData();

            $url = $feed->getUrl();

            // get feed if already entered
            $feed = $feedRepo->first(['url' => $url]);

            $feedReader = new Reader(new CurlDriver());
            $xml = $feedReader->load($url, $feed ? DateTime::createFromFormat('j-M-Y', $feed->getUpdatedAt()->format('j-M-Y')) : null);

            try {
                $feed = $feed ?: $form->getData();
                $feed->setUrl($url)
                    ->setUser($this->getUser())
                    ->setPublishedId($xml->getId())
                    ->setTitle($xml->getTitle())
                    ->setAuthors($xml->getAuthors())
                    ->setLinks($xml->getLinks())
                    ->setRights($xml->getSubtitle())
                    ->setCategories($xml->get('category'))
                    ->setContributors($xml->getContributors())
                    ->setGenerator($xml->getGenerator())
                    ->setIcon($xml->getIcon())
                    ->setLogo($xml->getLogo())
                    ->setRights($xml->getCopyright())
                    ->setFeedAttributes($xml->getFeedAttributes())
                    ->setUpdatedAt($xml->getLastModification())
                    ->setCreatedAt(new DateTime('now'));

                foreach ($xml->getItems() as $entry) {
                    $feedEntry = new FeedEntry();
                    $feedEntry->setPublishedId($entry->getId())
                        ->setTitle($entry->getTitle())
                        ->setAuthors($entry->getAuthors())
                        ->setContent($entry->getContent())
                        ->setSummary($entry->getSummary())
                        ->setLink($entry->getLinks())
                        ->setRights($entry->getSubtitle())
                        ->setCategories($entry->get('category'))
                        ->setContributors($entry->getContributors())
                        ->setRights($entry->getCopyright())
                        ->setSource($entry->getSource())
                        ->setUpdatedAt($entry->getLastModification());

                    $feed->addFeedEntry($feedEntry);
                    $em->persist($feedEntry);
                }

                $em->persist($feed);
                $em->flush();

                $this->addFlash('success', 'feed successfully created');
            } catch (NotModifiedResponseException $e) {
                $this->addFlash('info','feed not modified');
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('warning','feed entries already exists');
            }

            return $this->redirectToRoute('atom_feed_list');
        }

        return $this->render('@AtomFeed/user_feed/new.html.twig', [
            'feedForm' => $form->createView(),
            'error' => null,
        ]);
    }
}
