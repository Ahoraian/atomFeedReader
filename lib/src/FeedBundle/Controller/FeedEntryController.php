<?php

namespace Atom\FeedBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Atom\FeedBundle\Repository\FeedEntryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeedEntryController extends AbstractFeedController
{
    /**
     * @Route("/feeds", name="atom_feed_list")
     * @param Request $request
     * @param FeedEntryRepository $feedEntryRepo
     * @return Response
     */
    public function list(Request $request, FeedEntryRepository $feedEntryRepo): Response
    {
        $pageLimit = 30;
        $page = (int)$request->get('page') ?: 1;

        $userId = $this->getUser() ? $this->getUser()->getId() : null;

        // get all entries and paginate
        $feedEntries = $feedEntryRepo->paginate($userId, null, [
            'fe.updatedAt' => 'desc'
        ], $page, $pageLimit);

        return $this->render('@AtomFeed/feed_entry/index.html.twig', [
            'feedEntries' => $feedEntries,
            'page' => $page,
            'totalPage' => $feedEntries->count() ? ceil($feedEntries->count() / $pageLimit) : 0
        ]);
    }
}
