<?php

namespace Atom\FeedBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AtomFeedExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config['layouts'] as $name => $layout) {
            $config['layouts'][$name] = array_merge([
                'name' => $name,
                'assets_css' => [],
                'assets_js' => [],
                'host' => '',
                'pattern' => '',
            ], $layout);
            ksort($config['layouts'][$name]);
        }

        foreach ($config as $key => $value) {
            $container->setParameter('atom_feed.' . $key, $value);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
    }
}
