<?php

namespace Atom\FeedBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Atom\FeedBundle\Entity\Feed;
use Atom\FeedBundle\Entity\FeedEntry;
use Atom\FeedBundle\Entity\Page;
use Atom\FeedBundle\Entity\Category;
use Atom\FeedBundle\Entity\UserFeedEntry;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('atom_feed');
        $rootNode    = $treeBuilder->getRootNode();

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('feed_class')
                    ->isRequired()
                    ->validate()
                        ->ifString()
                        ->then(function($value) {
                            if (!class_exists($value) || !is_a($value, Feed::class, true)) {
                                throw new InvalidConfigurationException(sprintf(
                                    'Feed class must be a valid class extending %s. "%s" given.',
                                    Feed::class, $value
                                ));
                            }

                            return $value;
                        })
                    ->end()
                ->end()
                ->scalarNode('user_feed_entry_class')
                    ->isRequired()
                    ->validate()
                        ->ifString()
                        ->then(function($value) {
                            if (!class_exists($value) || !is_a($value, UserFeedEntry::class, true)) {
                                throw new InvalidConfigurationException(sprintf(
                                    'User Feed Entry class must be a valid class extending %s. "%s" given.',
                                    UserFeedEntry::class, $value
                                ));
                            }

                            return $value;
                        })
                    ->end()
                ->end()
                ->scalarNode('feed_entry_class')
                    ->isRequired()
                    ->validate()
                        ->ifString()
                        ->then(function($value) {
                            if (!class_exists($value) || !is_a($value, FeedEntry::class, true)) {
                                throw new InvalidConfigurationException(sprintf(
                                    'Feed Entry class must be a valid class extending %s. "%s" given.',
                                    FeedEntry::class, $value
                                ));
                            }

                            return $value;
                        })
                    ->end()
                ->end()
                ->arrayNode('layouts')
                    ->defaultValue([
                        'feed' => [
                            'resource' => '@AtomFeed/base.html.twig',
                            'pattern' => '',
                        ],
                    ])
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('resource')->isRequired()->end()
                            ->arrayNode('assets_css')->prototype('scalar')->end()->end()
                            ->arrayNode('assets_js')->prototype('scalar')->end()->end()
                            ->scalarNode('pattern')->defaultValue('')->end()
                            ->scalarNode('host')->defaultValue('')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('cache')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->integerNode('ttl')->defaultValue(300)->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
