<?php

namespace App\Entity;

use Atom\FeedBundle\Entity\UserFeedEntry as BaseUserFeedEntry;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Atom\FeedBundle\Repository\UserFeedEntryRepository")
 */
class UserFeedEntry extends BaseUserFeedEntry
{
}