<?php

namespace App\Entity;

use Atom\FeedBundle\Entity\FeedEntry as BaseFeedEntry;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Atom\FeedBundle\Repository\FeedEntryRepository")
 */
class FeedEntry extends BaseFeedEntry
{
}