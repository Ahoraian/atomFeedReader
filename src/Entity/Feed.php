<?php

namespace App\Entity;

use Atom\FeedBundle\Entity\Feed as BaseFeed;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Atom\FeedBundle\Repository\FeedRepository")
 */
class Feed extends BaseFeed
{

}